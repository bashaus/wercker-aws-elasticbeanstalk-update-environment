# Property: aws-access-key-id
# Check if the Access Key ID exists
if [[ -z "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_AWS_ACCESS_KEY_ID" ]];
then
  fail "Property aws-access-key-id or environment variable AWS_ACCESS_KEY_ID required"
fi

# Property: aws-secret-access-key
# Check if the Secret Access Key exists
if [[ -z "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_AWS_SECRET_ACCESS_KEY" ]];
then
  fail "Property aws-secret-access-key or environment variable AWS_SECRET_ACCESS_KEY required"
fi

# Store AWS details
mkdir -p $HOME/.aws/
echo "[default]" > $HOME/.aws/credentials
echo "aws_access_key_id = $WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_AWS_ACCESS_KEY_ID" >> $HOME/.aws/credentials
echo "aws_secret_access_key = $WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_AWS_SECRET_ACCESS_KEY" >> $HOME/.aws/credentials

# Property: application-name
# Ensure that a application-name has been provided
if [[ ! -n "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_APPLICATION_NAME" ]];
then
  fail "Property application-name must be defined"
fi

# Property: environment-name
# Ensure that a environment-name has been provided
if [[ ! -n "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_ENVIRONMENT_NAME" ]];
then
  fail "Property environment-name must be defined"
fi

##
debug "Check the environment is in a Ready state"

ATTEMPTS=10
for ATTEMPT in `seq 1 $ATTEMPTS`; do
  printf "Attempt #$ATTEMPT ... "

  WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_ENVIRONMENT_STATUS="$(
    aws elasticbeanstalk describe-environment-health \
      --region "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_AWS_REGION" \
      --environment-name "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_ENVIRONMENT_NAME" \
      --attribute-names All \
    | python \
      -c 'import sys, json; print(json.load(sys.stdin)["Status"])'
  )"

  echo "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_ENVIRONMENT_STATUS"

  case "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_ENVIRONMENT_STATUS" in
    "Ready" )
      break
      ;;

    "Launching" | "Updating" )
      continue
      ;;

    "Terminating" | "Terminated" | * )
      fail "Environment returned $WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_ENVIRONMENT_STATUS status"
      ;;
  esac

  if [[ $ATTEMPT == $ATTEMPTS ]]; then
    fail "Could not confirm Ready status after $ATTEMPTS attempts"
  else
    sleep 5
  fi
done

##
debug "Update ElasticBeanstalk Environment"

$(
  aws elasticbeanstalk update-environment \
    --region "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_AWS_REGION" \
    --application-name "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_APPLICATION_NAME" \
    --environment-name "$WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_ENVIRONMENT_NAME" \
    --version-label "$WERCKER_GIT_COMMIT"
)

##
debug "Delete credentials file"
rm $HOME/.aws/credentials
info "Deleted credentials file"

success "Environment $WERCKER_AWS_ELASTICBEANSTALK_UPDATE_ENVIRONMENT_ENVIRONMENT_NAME is updating"
