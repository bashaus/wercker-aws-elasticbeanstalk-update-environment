# AWS Elastic Beanstalk Update Environment

Takes an existing application version in ElasticBeanstalk and deploys it to an
environment.

## Notes

The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED",  "MAY", and
"OPTIONAL" in this document are to be interpreted as described in
RFC 2119.

## Sample Usage

You should use this step in conjunction with
`uk-hando/aws-elasticbeanstalk-create-application-version`.

    box: wercker/default
    deploy:
      box: ukhando/wercker-aws
      steps:
        - uk-hando/aws-elasticbeanstalk-create-application-version:
          application-name: $DEPLOY_APPLICATION_NAME
          s3-version-bucket: $DEPLOY_S3_VERSION_BUCKET

        - uk-hando/aws-elasticbeanstalk-update-environment:
          application-name: $DEPLOY_APPLICATION_NAME
          environment-name: $DEPLOY_ENVIRONMENT_NAME

Along with the available properties, ensure that you:

* Define the required environment variables in your application or pipeline
* Use the `ukhando/wercker-aws` docker container as your box

&nbsp;

## Dependencies

This step does not automatically install its dependencies to communicate with
Amazon Web Services. You must use the `ukhando/wercker-aws` box or something
similar.

&nbsp;

## Step Properties

### application-name (required)

The application name of the Elastic Beanstalk target

* Since: `0.0.1`
* Property is `Required`
* Recommended location: `Application`

&nbsp;

### environment-name (required)

Elastic Beanstalk environment name where the version will be deploy (e.g.
project-name-stag, project-name-prod)

* Since: `0.0.1`
* Property is `Required`
* Recommended location: `Pipeline`
* `Validation` rules:
  * Must be an environment of the `application-name` application

&nbsp;

### aws-access-key-id

The AWS_ACCESS_KEY_ID to use in this deployment

* Since: `0.1.2`
* Property is `Required`, but is `Optional` if `AWS_ACCESS_KEY_ID` is set
* Default value is: `AWS_ACCESS_KEY_ID`
* Recommended location: `Application`

&nbsp;

### aws-secret-access-key

The AWS_SECRET_ACCESS_KEY to use in this deployment

* Since: `0.1.2`
* Property is `Required`, but is `Optional` if `AWS_SECRET_ACCESS_KEY` is set
* Default value is: `AWS_SECRET_ACCESS_KEY`
* Recommended location: `Application`
* `Validation` rules:
  * Must be stored as a protected environment variable

&nbsp;

### aws-region

The region where the bucket is located. If not set, will use AWS_DEFAULT_REGION.
Most likely eu-west-1

* Since: `0.1.2`
* Property is `Required`, but is `Optional` if `AWS_DEFAULT_REGION` is set
* Default value is: `AWS_DEFAULT_REGION`
* Recommended location: `Application`

&nbsp;
